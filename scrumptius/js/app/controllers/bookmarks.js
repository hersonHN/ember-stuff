App.BookmarksController = Ember.ArrayController.extend({
    pushSort: function (attr) {

        if (this.get("sortProperties.firstObject") == attr) {
            this.toggleProperty("sortAscending");
        } else {
            this.set("sortProperties", [attr]);
            this.set("sortAscending", true);
        }
    }
});
